__WIP__  
This Repository is still a Work in Progress!

# MS COCO
[![Python][python-badge]][python-url]
[![PyTorch][pytorch-badge]][pytorch-url]
[![PL][pl-badge]][pl-url]
[![Lightnet][lightnet-badge]][lightnet-url]
[![Brambox][brambox-badge]][brambox-url]

For more information about how to use this repository,
check out our [tutorial](https://eavise.gitlab.io/lightnet/notes/03-B-ms_coco.html).


## Dependencies
This project requires a few python packages in order to work.  
First install [PyTorch](https://pytorch.org/get-started/locally) according to your machine specifications.
For this project we used PyTorch v1.7.0, but any higher version should work.  
Once PyTorch is installed, you can install the other dependencies by running the following command:
```bash
pip install -r requirements.txt
```


[python-badge]: https://img.shields.io/badge/python-3.6+-99CCFF.svg
[python-url]: https://python.org
[pytorch-badge]: https://img.shields.io/badge/PyTorch-1.7.0-F05732.svg
[pytorch-url]: https://pytorch.org
[pl-badge]: https://img.shields.io/badge/PyTorch%20Lightning-1.1-792EE5.svg
[pl-url]: https://www.pytorchlightning.ai
[lightnet-badge]: https://img.shields.io/badge/Lightnet-2.0.0-00BFD8.svg
[lightnet-url]: https://eavise.gitlab.io/lightnet
[brambox-badge]: https://img.shields.io/badge/Brambox-3.2.0-007EC6.svg
[brambox-url]: https://eavise.gitlab.io/brambox
