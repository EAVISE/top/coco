import torch
import lightnet as ln


def params(dataroot='data'):
    hp = ln.engine.HyperParameters()

    # Data params
    hp.data_folder = dataroot

    hp.class_label_map = [line.strip() for line in open(f'{hp.data_folder}/metadata/coco.names', 'r')]
    
    hp.mini_batch_size = 32
    
    hp.pre = ln.data.transform.Compose([
        ln.data.transform.Letterbox(dimension=(416, 416)),
        ln.data.transform.FitAnno(filter_threshold=0.01),
    ])
    
    hp.augment = {
        'jitter': .3,
        'flip': .5,
        'hue': .1,
        'saturation': 1.5,
        'value': 1.5,
    }
    
    
    # Model params
    def init_weights(m):
        if isinstance(m, torch.nn.Conv2d):
            torch.nn.init.kaiming_normal_(m.weight, nonlinearity='leaky_relu')
    
    hp.model = ln.models.YoloV2(
        len(hp.class_label_map),
        anchors=[(0.57273, 0.677385), (1.87446, 2.06253), (3.33843, 5.47434), (7.88282, 3.52778), (9.77052, 9.16828)]
    )

    hp.model.apply(init_weights)
    
    hp.loss = ln.network.loss.RegionLoss(
        len(hp.class_label_map),
        hp.model.anchors,
        hp.model.stride,
    )
    
    hp.post = ln.data.transform.Compose([
        ln.data.transform.GetDarknetBoxes(0.001, hp.model.stride, hp.model.anchors),
        ln.data.transform.NMS(0.5),
        ln.data.transform.TensorToBrambox(hp.class_label_map),
    ])

    # Set image_size to None, as it will be set in test_epoch_end
    hp.post_fit = ln.data.transform.Compose([
        ln.data.transform.ReverseLetterbox((416, 416), None)
    ])
    
    def optimizer(model):
        opt = torch.optim.SGD(
            model.parameters(),
            lr = .001,
            momentum = .9,
            weight_decay = .0005,
            dampening = 0,
        )
    
        burn_in = torch.optim.lr_scheduler.LambdaLR(
            opt,
            lambda b: (b / 2000) ** 4,
        )
        step = torch.optim.lr_scheduler.MultiStepLR(
            opt,
            milestones = [800000, 900000],
            gamma = .1,
        )
        sched = ln.engine.SchedulerCompositor(
            (0,     burn_in),
            (2000,  step),
        )
    
        return [opt], [{'scheduler': sched, 'interval': 'step'}]
    
    hp.optimizers = optimizer

    return hp
