import os
from pathlib import Path
import torch
import torchvision
import brambox as bb
import lightnet as ln
import pytorch_lightning as pl


class COCO17DataModule(pl.LightningDataModule):
    """ COCO 2017 Object Detection DataModule.
    This DataModule automatically converts all images to RGB and finally transforms them to a PyTorch tensor.
    Annotations are managed through Brambox and thus passed as Pandas.DataFrame object.
    For the training data, some extra data augmentation is also automatically added.

    Args:
        - params (lightnet.engine.HyperParameters): Hyperparameters for this data (see Note)

    Note:
        The params object needs the following attributes:

        - data_folder (string, Path-like): Path to the root data folder
        - class_label_map (list<string>): List of class_labels in the data
        - mini_batch_size (int): Batch size for the dataloaders
        - augment (dict, optional): Dictionary that defines the data augmentation during training. (See other Note)
        - pre (lightnet.data.transform.Compose, optional): Data transformation to apply to the images (See other Note)

    Note:
        By default this datamodule performs some data transformations, but you can add extra transforms by through ``params.pre``.
        The entire transformation pipeline for test/val looks as follows:
            - convert RGB: We convert all images to RGB, because some images are originally grayscale.
            - params.pre: Here you can define your own transforms. Usually this means one of the fitting transforms (Letterbox,Crop,Pad).
            - ToTensor: We transform the image to a PyTorch Tensor.

        For training, we automatically add some data augmentation: 
            - convert RGB: We convert all images to RGB, because some images are originally grayscale.
            - RandomHSV: Randomly modify HSV values.
            - RandomJitter: Randomly add jitter to the images (crop/add borders).
            - RandomFlip: Randomly flip the image horizontally.
            - params.pre: Here you can define your own transforms. Usually this means one of the fitting transforms (Letterbox,Crop,Pad)
            - ToTensor: We transform the image to a PyTorch Tensor.

        The 3 random transforms can be modified with the ``params.augment`` dictionary.
        If you do not pass a dict, or only pass a few values, these are the defaults:
        ```
        {
          'hue': 0.1,
          'sat': 1.5,
          'val': 1.5,
          'jitter': 0.3,
          'flip': 0.5
        }
        ```
    """
    def __init__(self, params, **kwargs):
        super().__init__()

        self.data_folder = Path(params.data_folder)
        self.tf = params.pre if hasattr(params, 'pre') else None
        self.clm = params.class_label_map
        self.batch_size = params.mini_batch_size

        augment = params.augment if hasattr(params, 'augment') else dict()
        self.hue = augment.get('hue', 0.1)
        self.saturation = augment.get('saturation', 1.5)
        self.value = augment.get('value', 1.5)
        self.jitter = augment.get('jitter', 0.3)
        self.flip = augment.get('flip', 0.5)
        self.kwargs = kwargs

        self.train = None
        self.val = None
        self.test = None

    def setup(self, stage=None):
        if stage == 'fit' or stage is None:
            train_anno = bb.io.load('anno_coco', self.data_folder / 'annotations/instances_train2017.json')
            self.train = ln.models.BramboxDataset(
                train_anno,
                class_label_map=self.clm,
                identify=lambda img: self.data_folder / f'train2017/{img}.jpg',
                transform=self.train_transforms,
            )
            self.train._name = 'COCO17_train'

            val_anno = bb.io.load('anno_coco', self.data_folder / 'annotations/instances_val2017.json')
            self.val = ln.models.BramboxDataset(
                val_anno, 
                class_label_map=self.clm,
                identify=lambda img: self.data_folder / f'val2017/{img}.jpg',
                transform=self.default_transforms,
            )
            self.val._name = 'COCO17_val'

        if stage == 'test' or stage is None:
            test_list = [f.name for f in os.scandir(self.data_folder / 'test2017') if f.is_file() and f.name.endswith('jpg')]
            test_anno = bb.util.new('anno', test_list)
            self.test = ln.models.BramboxDataset(
                test_anno,
                class_label_map=self.clm,
                identify=lambda img: self.data_folder / f'test2017/{img}.jpg',
                transform=self.default_transforms,
            )
            self.test._name = 'COCO17_test'

    def transfer_batch_to_device(self, batch, device):
        """ Our batches are tuples which contain: [Images Tensor, DataFrame] """
        img, df = batch
        return img.to(device), df

    def train_dataloader(self):
        if self.train:
            return torch.utils.data.DataLoader(
                self.train,
                self.batch_size,
                shuffle=True,
                drop_last=True,
                collate_fn=ln.data.brambox_collate,
                **self.kwargs
            )

    def val_dataloader(self):
        if self.val:
            return torch.utils.data.DataLoader(
                self.val,
                self.batch_size,
                collate_fn=ln.data.brambox_collate,
                **self.kwargs
            )

    def test_dataloader(self):
        if self.test:
            return torch.utils.data.DataLoader(
                self.test,
                self.batch_size,
                collate_fn=ln.data.brambox_collate,
                **self.kwargs
            )

    @property
    def default_transforms(self):
        pre = [
            lambda img: img.convert('RGB'),
        ]
        post = [
            torchvision.transforms.ToTensor()
        ]

        if self.tf is not None:
            return pre + self.tf + post
        else:
            return ln.data.transform.Compose(pre + post)

    @property
    def train_transforms(self):
        pre = [
            lambda img: img.convert('RGB'),
            ln.data.transform.RandomHSV(self.hue, self.saturation, self.value),
            ln.data.transform.RandomJitter(self.jitter),
            ln.data.transform.RandomFlip(self.flip),
        ]
        post = [
            torchvision.transforms.ToTensor()
        ]
        
        if self.tf is not None:
            return pre + self.tf + post
        else:
            return ln.data.transform.Compose(pre + post)
