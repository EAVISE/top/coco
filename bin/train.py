#!/usr/bin/env python
import argparse
import logging
import torch
import pytorch_lightning as pl
import lightnet as ln

from data import COCO17DataModule
from model import ObjectDetector


torch.set_num_threads(8)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Train network',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('params', help='Hyperparameter config file')
    parser.add_argument('weight', help='Path to pretrained weight file', default=None, nargs='?')
    parser.add_argument('-d', '--data', help='Root data folder', default='data')
    parser = pl.Trainer.add_argparse_args(parser)
    args = parser.parse_args()

    # Params
    params = ln.engine.HyperParameters.from_file(args.params, dataroot=args.data)
    params.model.load(args.weight, strict=False)

    data = COCO17DataModule(params, num_workers=10, pin_memory=True)
    model = ObjectDetector(params)

    # Trainer
    trainer = pl.Trainer.from_argparse_args(args)
    trainer.fit(model, datamodule=data)
