from PIL import Image
import pandas as pd
import brambox as bb
import pytorch_lightning as pl


class ObjectDetector(pl.LightningModule):
    """ Generic PL Object Detector Module. """
    def __init__(self, params):
        super().__init__()

        self.model = params.model
        self.loss = params.loss
        self.post = params.post
        self.post_fit = getattr(params, 'post_fit', None)
        self.optimizers = params.optimizers

    def configure_optimizers(self):
        return self.optimizers(self.model)

    def forward(self, *args, **kwargs):
        """ Pure model forward pass """
        return self.model(*args, **kwargs)

    def predict(self, *args, **kwargs):
        """ Forward and post-processing pass. """
        output = self(*args, **kwargs)
        return self.post(output)

    def training_step(self, batch, batch_idx):
        """ Run forward and compute loss. """
        data, gt = batch
        output = self(data)
        loss = self.loss(output, gt)
        self.log_dict({f'train_loss_{k}': v for k,v in self.loss.values.items()})
        return loss

    def validation_step(self, batch, batch_idx):
        """ Run forward, compute loss and save results and ground truth for mAP """
        data, gt = batch
        output = self(data)

        # Compute loss
        loss = self.loss(output, gt)
        self.log_dict({f'val_loss_{k}': v for k,v in self.loss.values.items()})

        # Postprocessing
        output = self.post(output)
        output.image = pd.Categorical.from_codes(output.image, dtype=gt.image.dtype)

        return {'loss': loss, 'det': output, 'anno': gt}

    def validation_epoch_end(self, outputs):
        """ Compute mAP from accumulated results and ground truth """
        det = bb.util.concat([o['det'] for o in outputs], ignore_index=True, sort=False)
        anno = bb.util.concat([o['anno'] for o in outputs], ignore_index=True, sort=False)
        coco = bb.eval.COCO(det, anno)
        self.log_dict({f'val_{k}': v for k, v in coco.mAP.iteritems()})

    def test_step(self, batch, batch_idx):
        """ Run forward and save results (and ground truth if available) """
        batch, gt = batch
        output = self.predict(batch)
        output.image = pd.Categorical.from_codes(output.image, dtype=gt.image.dtype)

        return {'det': output, 'anno': gt}
    
    def test_epoch_end(self, outputs):
        """ Save detections and compute mAP if test-set contained ground truth """
        det = bb.util.concat([o['det'] for o in outputs], ignore_index=True, sort=False)
        anno = bb.util.concat([o['anno'] for o in outputs], ignore_index=True, sort=False)

        # Compute mAP
        if len(anno) > 0:
            coco = bb.eval.COCO(det, anno)
            self.log_dict({f'test_{k}': v for k, v in coco.mAP.iteritems()})

        # Save detections
        dataset = self.trainer.test_dataloaders[0].dataset
        name = getattr(dataset, '_name', 'test')
        name = f'{self.trainer.log_dir}/{name}.h5'
        
        if self.post_fit is not None:
            self.post_fit[0].image_size = lambda name: Image.open(dataset.id(name)).size
            det = self.post_fit(det)

        bb.io.save(det, 'pandas', name)
        print(f'Saved detection results as "{name}"')

        # Save weights
        self.model.save(f'{self.trainer.log_dir}/{name}-weights.pt')
